#include <Arduino.h>
#include <bluefruit.h>
#include <Adafruit_NeoPixel.h>

BLEDis bledis;
BLEHidAdafruit blehid;

// 15 min sleep
#define SLEEP_DELAY 15*60*1000
unsigned long last_activity = 0;

#define DEBUG_PIXEL 0

#if DEBUG_PIXEL
Adafruit_NeoPixel neopixel = Adafruit_NeoPixel(1, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ800);
#endif

void startAdv() {
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);

  Bluefruit.Advertising.addService(blehid);
  Bluefruit.Advertising.addName();

  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244); // in unit of 0.625ms
  Bluefruit.Advertising.setFastTimeout(30);
  Bluefruit.Advertising.start(0);
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_A0, INPUT_PULLUP);
  digitalWrite(LED_BUILTIN, LOW);

#if DEBUG_PIXEL
  neopixel.begin();
  neopixel.setBrightness(40);
  neopixel.setPixelColor(0, neopixel.Color(150, 0, 150));
  neopixel.show();
#endif

  Bluefruit.begin();
  Bluefruit.setTxPower(4);
  Bluefruit.setName("Ctrl-Pedal");
  
  bledis.setManufacturer("VeryCuteDog Labs");
  bledis.setModel("Ctrl-Pedal A");
  bledis.begin();

  blehid.begin();
  //blehid.setKeyboardLedCallback(set_keyboard_led);
  startAdv();
}

hid_keyboard_report_t report =
  {
   .modifier = KEYBOARD_MODIFIER_LEFTCTRL,
  };

#if DEBUG_PIXEL
void sleepIndication() {
  neopixel.setPixelColor(0, neopixel.Color(0, 150, 0));
  neopixel.show();
  delay(1000);
  neopixel.clear();
}

void wakeUpIndication() {
  neopixel.setPixelColor(0, neopixel.Color(0, 0, 150));
  neopixel.show();
  delay(1000);
  neopixel.clear();
}
#endif

void goToSleep() {
  if (millis() - last_activity > SLEEP_DELAY) {
#if DEBUG_PIXEL
    sleepIndication();
#endif
    // turn off conn led
    digitalWrite(LED_CONN, LOW);
    systemOff(PIN_A0, 0);
#if DEBUG_PIXEL
    wakeUpIndication();
#endif
  }
}

void loop() {
  if (digitalRead(PIN_A0)) {
    blehid.keyRelease();
  } else {
    blehid.keyboardReport(&report);
    last_activity = millis();
  }
  goToSleep();
  delay(10);
}
